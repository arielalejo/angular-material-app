import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatComponentsModule } from './mat-components.module';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { SelectComponent } from './select/select.component';
import { RadioButtonComponent } from './radio-button/radio-button.component';
import { DialogComponent, DialogContent, DIALOG_DATA } from './dialog/dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    CheckboxComponent,
    RadioButtonComponent,
    SelectComponent,
    DialogComponent,
    DialogContent
  ], 
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatComponentsModule
  ],
  providers: [
    {provide: DIALOG_DATA, useValue: {}} 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
