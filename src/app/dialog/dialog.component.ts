import { Component, OnInit, Inject, InjectionToken } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData{
  animal: 'panda' | 'unicorn' | 'lion'
}

/* *********************************************************************** */

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(){
    const dialogRef = this.dialog.open(DialogContent, {
      data: "uno"
      // data: {
      //   animal: 'panda'
      // }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('dialog result: ', result)
    })
  }

}

/* *********************************************************************** */
export const DIALOG_DATA = new InjectionToken("MY_DIALOG_DATA");

@Component({
  selector: 'dialog-content',
  templateUrl: './dialog-content.html'
})
export class DialogContent{
  constructor(@Inject(MAT_DIALOG_DATA) public data: any){
    console.log("data:" , data)
  }
}